import { User } from './../user';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  myUser: User;
  myId = 1;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.myUser = this.dataService.getUserById(this.myId);
  }

}
