import { User } from './user';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private users: User[] = [
    {id: 1, name: 'BA', firstName: 'Souleymane',  function: 'Développeur logiciel', role: 'Support et process outils R&D'}
];

  constructor() { }

  getUserById(id: number) {
    return this.users.find(user => user.id === id);
  }
}
