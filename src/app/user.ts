

export interface User {
    id: number;
    name: string;
    firstName: string;
    function: string;
    role: string;
}
